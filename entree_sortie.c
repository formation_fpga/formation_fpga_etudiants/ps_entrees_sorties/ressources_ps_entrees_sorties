/*Création Jérémy Bolloré 03/10/2019
 modification 05/10/2021*/


//Driver GPIO
#include "xgpio.h"
//envoi trame UART
#include "xil_printf.h"
//Information sur les périphériques inclus dans le projet ( voir design)
#include "xparameters.h"
// gestion des temporistions
#include "sleep.h"

/**** programme principal******/
int main()
{
	u32 etat_bouton; // déclaration variables globales 
	XGpio btn;       // déclaration variable périphériques boutons
	XGpio leds;      // déclaration variable périphériques leds

	XGpio_Initialize(&btn, XPAR_BOUTONS_DEVICE_ID); // Initialisations des périphériques boutons
	XGpio_Initialize(&leds, XPAR_LEDS_DEVICE_ID);  // Initialisations des périphériques leds


	while (1)
	{
		etat_bouton=XGpio_DiscreteRead(&btn,1); // lecture bouton
	    if (etat_bouton==1)
	    {
	    	XGpio_DiscreteWrite(&leds, 1, 0xFFFF);// valeur des leds  l'instant t


	    }
		else
			XGpio_DiscreteWrite(&leds, 1, 0x0000);
	}
}


